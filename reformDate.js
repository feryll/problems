function reformatDate(dates) {
    // Write your code here
    var month = { 'Jan': "01", 'Feb': "02", 'Mar': "03", "Apr": "04", "May": "05", "Jun": "06", "Jul": "07", "Aug": "08", "Sep": "09", "Oct": "10", "Nov": "11", "Dec": "12" };
    var ret = [];
    for (var date of dates) {
        var tmp = date.split(" ");
        tmp[0] = tmp[0].replace(/[^0-9]/g, "");
        tmp[0] = tmp[0] > 9 ? tmp[0] : "0" + tmp[0];
        tmp[1] = month[tmp[1]];
        tmp = tmp.reverse().join('-');
        ret.push(tmp);
    }
    return ret;
}
function rotLeft(a, d) {
    var ret = [];
    for(var i=1;i<=a;i++) {
        ret.push(i);
    }
    for(var i=0;i<d;i++) {
        var tmp = ret.shift();
        ret.push(tmp);
    }
    return ret;
}

console.log(rotLeft(5,4));
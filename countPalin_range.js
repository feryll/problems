function solution(n, m){
    var answer = 0
    var reverse_num=0,num,temp,rem;
    for(num=n;num<=m;num++){
      temp=num;
      reverse_num=0;
      while(temp > 0){
        rem=Math.floor(temp%10);
        temp=Math.floor(temp/10);
        reverse_num=Math.floor(reverse_num*10)+rem;
      }
      if(num==reverse_num) {
        answer++;
      }
   }
    
    return answer;
}

console.log(solution(1,1300));


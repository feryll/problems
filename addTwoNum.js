/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */
/**
 * 
 * You are given two non-empty linked lists representing two non-negative integers. 
 * The digits are stored in reverse order and each of their nodes contain a single digit. 
 * Add the two numbers and return it as a linked list. 
 * You may assume the two numbers do not contain any leading zero, except the number 0 itself.
 * 
 * @param {ListNode} l1
 * @param {ListNode} l2
 * @return {ListNode}
 */

var addTwoNumbers = function(l1, l2) {
    var list = new ListNode(0);
    var l3 = list;
    var sum = 0;
    while(l1 || l2) {
        sum = Math.floor(sum/10);
        if(l1) {
            sum += l1.val;
            l1 = l1.next;
        }
        if(l2) {
            sum += l2.val;
            l2 = l2.next;
        }
        
        l3.next = new ListNode(sum % 10);
        l3 = l3.next;
    }
    if(Math.floor(sum/10) == 1) l3.next = new ListNode(1);
   
    return list.next;
};
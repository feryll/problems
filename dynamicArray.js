//https://www.hackerrank.com/challenges/dynamic-array/problem?utm_campaign=challenge-recommendation&utm_medium=email&utm_source=24-hour-campaign
function dynamicArray(n, queries) {
    var seqList = [];
    var lastAnswer = 0;
    var ret = [];
    for (var i = 0; i < n; i++){
        seqList[i] = [];
    }
    for (var i = 0; i < queries.length; i++) {
        if (queries[i][0] == 1) {
            var index = (queries[i][1] ^ lastAnswer) % n;
            seqList[index].push(queries[i][2]);
        }
        else if(queries[i][0] == 2) {
            var index = (queries[i][1] ^ lastAnswer) % n;
            var seq_index = queries[i][2] % seqList[index].length;
            lastAnswer = seqList[index][seq_index];
            ret.push(lastAnswer);
        }
    }
    return ret;
}

//console.log(dynamicArray([2,1,5,3,4]));
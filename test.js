/**
 * @param {number[]} nums
 * @return {number[][]}
 */
var threeSum = function(nums) {
       
    nums.sort((a,b) => a - b );
    var res = [];
    
    for(var i=0;i<nums.length-2;i++) {
        var start = i + 1;
        var end = nums.length - 1;

        while(start < end) {
            var tmp = nums[i] + nums[start] + nums[end];
            if(0 == tmp) {
                res.push([nums[i],nums[start],nums[end]]);
                while (start < end && nums[start] == res[res.length-1][1]) start++;
                while (start < end && nums[end] == res[res.length-1][2]) end--;
            }
            if(tmp < 0) start++;
            else if(tmp > 0) end--;
        }

        while (i + 1 < nums.length && nums[i + 1] == nums[i]) i++;
    }

    return res;
};



console.log(threeSum([0,0,0,0]));
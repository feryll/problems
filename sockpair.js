

// Complete the sockMerchant function below.
function sockMerchant(n, ar) {

    var counter = [];
    for (var i = 0; i < n; i++) {
        if (counter[ar[i]] == undefined) counter[ar[i]] = 1;
        else counter[ar[i]]++;
    }

    var sum = 0;

    for (var color in counter) {
        sum += Math.floor(counter[color] / 2);
    }
    return sum;
}

console.log(sockMerchant(9,[10,20,20,10,10,30,50,10,20]));
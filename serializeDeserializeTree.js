/**
 * Definition for a binary tree node.
 */
function TreeNode(val) {
    this.val = val;
    this.left = this.right = null;
}
   
/**
 * Encodes a tree to a single string.
 *
 * @param {TreeNode} root
 * @return {string}
 */
var serialize = function(root) {
    var ret = "";
    function buildString(node) {
        if(node == null) {
            ret = ret + "n" + ' ';
        } else {
            ret = ret + node.val + ' ';
            buildString(node.left);
            buildString(node.right);
        }
    }
    buildString(root, ret);
    return ret;
};

/**
 * Decodes your encoded data to tree.
 *
 * @param {string} data
 * @return {TreeNode}
 */
var deserialize = function(data) {
    data = data.split(' ');
    function buildTree(pool) {
        var tmp = pool.shift();
        if(tmp == 'n') return null;
        else {
            var node = new TreeNode(tmp);
            node.left = buildTree(pool);
            node.right = buildTree(pool);
            return node;
        }
    }
    return buildTree(data);
};

var root = new TreeNode(1);
root.left = new TreeNode(2);
root.right = new TreeNode(3);
root.right.left = new TreeNode(4);
root.right.right = new TreeNode(5);
// console.log(root);
console.log(deserialize(serialize(root)));
 
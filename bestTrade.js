// best trade - divide and conquer sample
var trade = function(prices) {
    if(prices.length == 1) return 0;
    var divider = Math.floor(prices.length/2);
    var firsthalf = prices.slice(0, divider);
    var secondhalf = prices.slice(divider);
    var case3 = Math.max(...secondhalf) - Math.min(...firsthalf);
    return Math.max(case3, trade(firsthalf), trade(secondhalf));
}

console.log(trade([27,53,7,25,33,2,32,47,43]));
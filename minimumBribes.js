function minimumBribes(q) {
    var count = 0;

    for (var i = q.length - 1; i >= 0; i--) {
        
        if (q[i] - (i + 1) > 2) {
            process.stdout.write("Too chaotic" + '\n');
            return ; 
        }
        for (var j = Math.max(0, q[i] - 2); j < i; j++)
            if (q[j] > q[i]) count++;
    }
    process.stdout.write(count + '\n');
    return;
}

console.log(minimumBribes([2,1,5,3,4]));
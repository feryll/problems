/**
 * @param {string} str
 * @return {number}
 */
var myAtoi = function(str) {
    var NUMERIC_REGEXP = /^[+-]?[\d]+/g;
    var clone = str.trim();
    clone = clone.match(NUMERIC_REGEXP);
    if(clone == null) clone = 0;
    if(clone < -0x80000000) clone = -0x80000000;
    if(clone > 0x7fffffff) clone = 0x7fffffff;
    return clone
};

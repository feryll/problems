// mergesort - divide and conquer sample
var mergesort = function(nums) {
    if(nums.length == 1) return nums;
    var divider = Math.floor(nums.length/2);
    var firsthalf = nums.slice(0, divider);
    var secondhalf = nums.slice(divider);
    return merge(mergesort(firsthalf), mergesort(secondhalf));
}

var merge = function(nums1, nums2) {
    var ret = [];

    while(nums1.length > 0 && nums2.length > 0) {
        if(nums1[0] < nums2[0]) {
            ret.push(nums1.shift());
        }
        else {
            ret.push(nums2.shift());
        }
    }
    while(nums1.length > 0)  ret.push(nums1.shift());
    while(nums2.length > 0)  ret.push(nums2.shift());
    return ret;
}

console.log(mergesort([0,5,6,8,9,0,4,32,1,1,4,5,3,54,6,7,8,9,63,2]));
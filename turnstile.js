'use strict';

const fs = require('fs');

process.stdin.resume();
process.stdin.setEncoding('utf-8');

let inputString = '';
let currentLine = 0;

process.stdin.on('data', function(inputStdin) {
    inputString += inputStdin;
});

process.stdin.on('end', function() {
    inputString = inputString.split('\n');

    main();
});

function readLine() {
    return inputString[currentLine++];
}



/*
 * Complete the 'getTimes' function below.
 *
 * The function is expected to return an INTEGER_ARRAY.
 * The function accepts following parameters:
 *  1. INTEGER_ARRAY time
 *  2. INTEGER_ARRAY direction
 */

function getTimes(time, direction) {
    var merged = [];
    var leaveQueue = [];
    var enterQueue = []; 

    for (var i = 0; i < time.length; i++) {
        var person = [i, time[i]];
        if (direction[i] == 0) {
            enterQueue.push(person); 
        } else {
            leaveQueue.push(person);
        }
    }

    var prevDir = 1, passed = 0;

    while (enterQueue.length > 0 && leaveQueue.length > 0) {
        if (enterQueue[0][1] < passed) enterQueue[0][1] = passed;
        if (leaveQueue[0][1] < passed) leaveQueue[0][1] = passed;

        if (enterQueue[0][1] == leaveQueue[0][1]) {
            // console.log("length", merged.length);
            // console.log("now", enterQueue[0][1]);
            // console.log("prev", merged[merged.length - 1][1]);
            if (merged.length > 0 && (enterQueue[0][1] - merged[merged.length - 1][1]) > 1) {
                // bug fixed
                prevDir = 1;
            }
            if (prevDir == 1) {
                merged.push(leaveQueue.shift());
                prevDir = 1;
                passed = merged[merged.length - 1][1];
                //console.log("leave");
            }
            else {
                
                merged.push(enterQueue.shift());
                prevDir = 0;
                passed = merged[merged.length - 1][1];
                //console.log("enter");
            }
        }
        else if (enterQueue[0][1] > leaveQueue[0][1]) {
            merged.push(leaveQueue.shift());
            prevDir = 1;
            passed = merged[merged.length - 1][1];
            //console.log("leave2");
        }
        else if (enterQueue[0][1] < leaveQueue[0][1]) {
            merged.push(enterQueue.shift());
            prevDir = 0;
            passed = merged[merged.length - 1][1];
            //console.log("enter2");
        }
        passed++;
    }

    while (enterQueue.length > 0) {
        if (enterQueue[0][1] <= passed) enterQueue[0][1] = passed++;
        merged.push(enterQueue.shift());
        
    }

    while (leaveQueue.length > 0) {
        if (leaveQueue[0][1] <= passed) leaveQueue[0][1] = passed++;
        merged.push(leaveQueue.shift());
    }

    merged = merged.sort(function (a, b) {
        return a[0] - b[0];
    }).map((val) => val[1]);

    return merged;
}
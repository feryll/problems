function repeatedString(s, n) {
    var rem_length = n%s.length;
    var remnant = 0;
    var whole = 0;
    for(var i=0;i<s.length;i++) {
        if(s[i] == 'a') whole++;
    }
    for(var i=0;i<rem_length;i++) {
        if(s[i] == 'a') remnant++;
    }
    return Math.floor(n/s.length)*whole + remnant;
}


console.log(repeatedString("aba", 10));
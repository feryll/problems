//Given a string, find the length of the longest substring without repeating characters.

/**
 * @param {string} s
 * @return {number}
 */
var lengthOfLongestSubstring = function(s) {
    var charset = [];
    var max = 0;
    for(var i=0,j=0;i<s.length;i++) {
        if(charset[s[i]] != undefined) {
            j = Math.max(j, charset[s[i]]+1);
        }
        charset[s[i]] = i;
        max = Math.max(max, i-j+1);
    }
    return max;
    
};
console.log(lengthOfLongestSubstring("abcdba"));
function countingValleys(n, s) {
    var valley = 0;
    var start = 0;
    while(start < n) {
        var tmp = checkValley(n,s,start);
        start = tmp[1];
        valley += tmp[0];
    } 

    return valley;
}

function checkValley(n,s,start) {
    var altitude = 0;
    console.log(n,s,start);
    for(var i = start;i<n;i++) {
        if(s[i] == 'U') altitude++;
        else if(s[i] == 'D') altitude--;
        if(altitude == 0) return [s[start] == 'D' ? 1 : 0, i+1];  
    }
    return [0, n];
}

console.log(countingValleys(8,"UDDDUDUU"));
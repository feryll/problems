// Given a string s, find the longest palindromic substring in s. You may assume that the maximum length of s is 1000.

// Example 1:

// Input: "babad"
// Output: "bab"
// Note: "aba" is also a valid answer.

// Example 2:

// Input: "cbbd"
// Output: "bb"

function findPalin(s, left, right)
{

    while(left >= 0 && right < s.length && s[left] == s[right]) {
        left--;
        right++;
    }
    return right - left -1;
}

var longestPalindrome = function(s) {

    var left = 0, right = 0;
    for(i=0;i< s.length; i++) {
        var len1 = findPalin(s, i, i);
        var len2 = findPalin(s, i, i+1);
        var len = Math.max(len1,len2);
        if(len > right - left) {
            left = i - Math.floor((len-1)/2);
            right = i + Math.floor(len/2);
        }
    }

    return s.substring(left, right+1);  
};
console.log(longestPalindrome("cbbd"));


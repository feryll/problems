function hourglassSum(arr) {
    var max = -64;
    for(var i=1;i<4;i++) {
        for(var j=1;j<4;j++) {
            max = Math.max(max, calculateHourglassSum(arr, i, j));
        }    
    }
    return max;
}

function calculateHourglassSum(arr, x, y) {
    return arr[x][y] + arr[x-1][y-1] + arr[x-1][y] + arr[x-1][y+1] + arr[x+1][y-1] + arr[x+1][y] + arr[x+1][y+1];
}

console.log(hourglassSum(
    [
        [-1, -1, 0, -9, -2, -2],
        [-2, -1, -6, -8, -2, -5],
        [-1, -1, -1, -2, -3, -4],
        [-1, -9, -2, -4, -4, -5],
        [-7, -3, -3, -2, -9, -9],
        [-1, -3, -1, -2, -4, -5]
    ]));
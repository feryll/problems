
function protectionTime(startingPos, speed) {
    // 1. approach: brute force. too slow to solve case 11.
    // var count = 1;
    // var landed = 0;
    // while (count > landed && count < speed.length) {
    //     landed = 0;
    //     for (var i = 0; i < startingPos.length; i++) {
    //         startingPos[i] = startingPos[i] - speed[i];
    //         if (startingPos[i] <= 0) landed++;
    //     }
    //     count++;
    // }
    // // if all bogeys are hit, return count
    // return (landed == 0) ? count : count -1;

    // 2. approach: calculate the turns of landing for each bogeys first 
    //    and count when the landed are more than the number of turns passed.
    var doomCount = [];
    for (var i = 0; i < startingPos.length; i++) {
        var turnOfDoom = Math.ceil(startingPos[i]/speed[i]);
        if (doomCount[turnOfDoom] == undefined) doomCount[turnOfDoom] = 1;
        else doomCount[turnOfDoom]++;
    }
    
    var landed = 0;
    for (var turn in doomCount) {
        console.log(turn, doomCount[turn]);
        landed += doomCount[turn];
        if (landed > turn) return turn;
    }
    // killed them all! Survived!
    return speed.length;
}

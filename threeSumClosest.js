/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number}
 */
var threeSumClosest = function(nums, target) {
    
    nums.sort((a,b) => a - b );
    var res = nums[0] + nums[1] + nums[2];
    
    for(var i=0;i<nums.length-2;i++) {
        var start = i + 1;
        var end = nums.length - 1;

        while(start < end) {
            var tmp = nums[i] + nums[start] + nums[end];
            if(target == tmp) return target;
            if(Math.abs(target - res) > Math.abs(target - tmp)) res = tmp;
            if(tmp < target) start++;
            else if(tmp > target) end--;
        }    
    }

    return res;
};

console.log(threeSumClosest([-1,2,1,-4], 1));